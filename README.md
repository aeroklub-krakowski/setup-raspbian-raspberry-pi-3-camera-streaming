# Purpose

Collect video streams from connected cameras by rstp protocol and publish them on website hosted on this machine.

# Goals

* minimum cpu usage
* minimum ram usage
* minimum power usage
* workspace in ram to save flash write cycles

# Implementation

Gstreamer is used to get video stream from camera. Protocol is rstp with authentication credentials read from prepared file with secrets. Video stream encoded in h264 standard is extracted and split into smaller video files as mp4 containers. Only few last video files are kept. Playlist with order of these video files is generated regularly on each new video update. Playlist is in Http Live Streaming format.

Each camera has its own gstreamer working directory with video files and playlist. These directories are published by nginx http server. Also it is published demo website with video players of these videos.

Client browser should support native h264 video decoding and expose it by Media Source Extension javascript framework.

# Installation

Copy templates with cameras credentials keeping access secure and creating directories if needed
```
install --mode=u=rw,g-rwx,o-rwx -D ./filesystem/root/secret/camera/east.template /root/secret/camera/east
install --mode=u=rw,g-rwx,o-rwx -D ./filesystem/root/secret/camera/west.template /root/secret/camera/west
```

Set credentials to right values.

Then install necessary software dependencies and custom configuration
```
./install
```

# Demo

Website with demo should be available on address specified in file with secret credentials.

# Troubleshooting

Ensure that gstreamer processes and nginx are running fine without any error
```
systemctl status camera@* nginx
```

Take a look at logs
```
sudo journalctl --unit camera@east
sudo journalctl --unit camera@west
sudo journalctl --unit nginx
```
